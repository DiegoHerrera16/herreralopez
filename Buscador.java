public class Buscador <E>{
	Comparable<E>[] elementos;
	Buscador(Comparable<E>[] elementos){
		this.elementos = elementos;
	}
	int HacerBusquedaSecuencial(E ElementoBuscado) {
		for( int i=0;  i< elementos.length; i++) {
			if(elementos[i].compareTo(ElementoBuscado)  ==0) {
				return i;
			}
		}
		return -1;

	}
	int HacerBusquedaBinaria(E ElementoBuscado) {
		  int n = elementos.length;
		  int centro,inf=0,sup=n-1;
		   while(inf<=sup){
		     centro=(sup+inf)/2;
		     if(elementos[centro]==ElementoBuscado) return centro;
		     else if(elementos[centro].compareTo(ElementoBuscado)>0 ){
		        sup=centro-1;
		     }
		     else {
		       inf=centro+1;
		     }
		   }
		return -1;
	}
}