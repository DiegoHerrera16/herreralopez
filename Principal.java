public class Principal {

	public static void main(String[] args) {
		String []palabras = {"Aries","Azar", "Cabernicola","Diamante","Domar"};
		Buscador<String> Busca = new Buscador<>(palabras);
		if(Busca.HacerBusquedaBinaria("Diamante")!=-1) {
			System.out.println("encontrado");
		}
		if(Busca.HacerBusquedaSecuencial("Diamante")!=-1) {
			System.out.println("encontrado");
		}
	}

}